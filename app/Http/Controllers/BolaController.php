<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bola;
use App\Standing;

class BolaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('clubname')){
            $standing = Standing::where('club_name','LIKE','%'.$request->clubname.'%')->get();
            return $standing;  
        }
        else{
            return Standing::orderBy('points', 'desc')->get();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $bola = new Bola;
        $standings = Standing::all();
        $bola->club_home_name= $request->club_home_name;
        $bola->club_away_name= $request->club_away_name;
        $bola->score_home = $request->score_home;
        $bola->score_away = $request->score_away;
        if($bola->score_away == $bola->score_home){
            $bola->point_home = 1;
            $bola->point_away = 1;
        }
        elseif($bola->score_away > $bola->score_home){
            $bola->point_home = 0;
            $bola->point_away = 3;
        }
        elseif($bola->score_away < $bola->score_home){
            $bola->point_home = 3;
            $bola->point_away = 0;
        }
        foreach($standings as $standing){
            if($standing->club_name == $bola->club_home_name){
                $standing->points += $bola->point_home;
            }
            elseif($standing->club_name == $bola->club_away_name){
                $standing->points += $bola->point_away;
            }
            $standing->save();
        }
        $bola->save();
        return response()->json([
            'success' => true,
            'message' => 'Match Success Created'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
