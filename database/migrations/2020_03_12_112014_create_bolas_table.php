<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBolasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bolas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('club_home_name');
            $table->string('club_away_name');
            $table->integer('score_home');
            $table->integer('score_away');
            $table->integer('point_home');
            $table->integer('point_away');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bolas');
    }
}
